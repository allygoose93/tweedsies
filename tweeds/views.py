from django.shortcuts import render, redirect
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.decorators.http import require_http_methods
from sqlite3 import IntegrityError
from tweeds.models import Post, Comment


# Create your views here.
class PostListView(ListView):
    model = Post
    template_name = "posts/list.html"



@require_http_methods(["POST"])
def create_post(request):
    # get text from post request
    post_text = request.POST.get("post_text")
    # get user id
    requested_user = request.user

    try:
        # create post list item
        Post.objects.create(
            owner=requested_user, text=post_text
        )
    except IntegrityError:
        pass
    # redirect to current recipe page-which we get from the request
    return redirect("home")



@require_http_methods(["POST"])
def create_comment(request):
    # get comment from post request
    comment_text = request.POST.get("comment_text")
    post_id = request.POST.get("feed_post_id")
    post = Post.objects.get(id=post_id)
    print("thisssssss", post_id, comment_text)
    # get user id
    requested_user = request.user

    try:
        # create shopping list item
        Comment.objects.create(
            owner=requested_user, text=comment_text, post = post
        )
    except IntegrityError:
        pass
    # redirect to current recipe page-which we get from the request
    return redirect("home")