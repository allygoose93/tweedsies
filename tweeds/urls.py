from django.urls import path

from tweeds.views import (
    PostListView,
    create_post,
    create_comment
)

urlpatterns = [
    path("", PostListView.as_view(), name='home'),
    path("create/", create_post, name='create_post'),
    path("create/comment/", create_comment, name='create_comment')
]
