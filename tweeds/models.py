from django.db import models
from django.conf import settings


USER = settings.AUTH_USER_MODEL

# Create your models here.
class Post(models.Model):
    text = models.TextField()
    owner = models.ForeignKey(USER, related_name='posts', on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-created']

    def __str__(self):
        return self.text

class Comment(models.Model):
    text = models.TextField()
    owner = models.ForeignKey(USER, related_name='comments', on_delete=models.CASCADE)
    post = models.ForeignKey("Post", related_name='comments', on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.text